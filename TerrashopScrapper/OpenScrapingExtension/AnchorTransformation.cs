﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using OpenScraping.Transformations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrashopScrapper
{
    public class AnchorTransformation : ITransformationFromHtml
    {
        bool isCategory = false;

        public object Transform(Dictionary<string, object> settings, HtmlNode node, List<HtmlNode> logicalParents)
        {
            if(node.Attributes!=null)
            {
                var attrib = node.Attributes["href"];
                return attrib.Value;
            }
            return null;
        }
    }
}
