﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using OpenScraping.Transformations;
using System;
using System.Collections.Generic;

namespace TerrashopScrapper
{
    public class NavbarTransformation : ITransformationFromHtml
    {
        bool isCategory = false;

        public object Transform(Dictionary<string, object> settings, HtmlNode node, List<HtmlNode> logicalParents)
        {
            JArray resp = new JArray();

            foreach (var cNode in node.ChildNodes)
            {
                if(cNode.Name.SafeLower() =="div")
                {
                    isCategory = cNode.InnerHtml == "Kategorie";
                }

                if(cNode.Name.SafeLower() == "a" && isCategory)
                {
                    var attrib = cNode.Attributes["href"];

                    JObject data = new JObject();
                    data.Add("name", cNode.InnerText);
                    data.Add("url", attrib.Value);
                    resp.Add(data);
                }
            }

            return resp;
        }
    }
}
