﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrashopScrapper
{
    public static class ExtensionMethods
    {
        public static string SafeTrim(this string input)
        {
            if(string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            return input.Trim();
        }

        public static string SafeLower(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            return input.ToLower();
        }
    }
}
