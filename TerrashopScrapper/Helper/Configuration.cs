﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrashopScrapper
{
    public static class Configuration
    {
        private static string baseUrl;

        public static string BaseUrl
        {
            get
            {
                if (baseUrl == null)
                {
                    baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
                }

                return baseUrl;
            }
        }
    }
}
