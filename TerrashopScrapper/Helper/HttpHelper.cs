﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TerrashopScrapper
{
    public class HttpHelper
    {
        HttpClient _client = null;

        public HttpHelper()
        {
            _client = new HttpClient();
        }

        public string Get(string url)
        {
            try
            {
                var response = _client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    var response1 = _client.GetAsync(url).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        return response1.Content.ReadAsStringAsync().Result;
                    }
                }
            }
            catch(Exception ex)
            {
                var response2 = _client.GetAsync(url).Result;
                if (response2.IsSuccessStatusCode)
                {
                    return response2.Content.ReadAsStringAsync().Result;
                }
            }

            return null;
        }
    }
}
