﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrashopScrapper
{
    public class ProductDetail
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Price { get; set; }
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string EAN { get; set; }
    }
}
