﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using OpenScraping;
using OpenScraping.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrashopScrapper
{
    public class Parser
    {
        HttpHelper helper = new HttpHelper();

        public void Parse()
        {
            var relativeUrl = "restposten.php";
            var res = ParseProducts(relativeUrl);

            UpdateProductDetail(res);

            var wb = new XLWorkbook();

            var dataTable = GetTable(res);

            wb.Worksheets.Add(dataTable);

            wb.SaveAs("AddingDataTableAsWorksheet.xlsx");

           
            //var relativeUrl1 = "restposten.php";
            //var res1 = ParseProducts(relativeUrl);
        }

        private DataTable GetTable(List<ProductDetail> res)
        {
            DataTable table = new DataTable();
            table.TableName = "result";
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Url", typeof(string));
            table.Columns.Add("Price", typeof(string));
            table.Columns.Add("ISBN10", typeof(string));
            table.Columns.Add("ISBN13", typeof(string));
            table.Columns.Add("EAN", typeof(string));

            foreach (var item in res)
            {
                table.Rows.Add(item.Name, item.Url, item.Price, item.ISBN10, item.ISBN13, item.EAN);

            }

            return table;
        }

        private void UpdateProductDetail(List<ProductDetail> res)
        {
            Parallel.ForEach(res, new ParallelOptions() { MaxDegreeOfParallelism = 100 }, product =>
            {
                var data = helper.Get(Configuration.BaseUrl + product.Url);

                if (data != null)
                {
                    var config = StructuredDataConfig.ParseJsonString(File.ReadAllText(@"Config\item.json"));

                    var openScraping = new StructuredDataExtractor(config);
                    var scrapingResults = openScraping.Extract(data);

                    var json = JsonConvert.SerializeObject(scrapingResults, Formatting.Indented);

                    var parsedJson = JsonConvert.DeserializeObject<Item>(json) ?? new Item();

                    product.Name = parsedJson.Title;
                    product.Price = parsedJson.Price;
                    product.ISBN10 = GetISBNrEAN(parsedJson.ISBN, "ISBN-10:", 10);
                    product.ISBN13 = GetISBNrEAN(parsedJson.ISBN, "ISBN-13:", 13);
                    product.EAN = GetISBNrEAN(parsedJson.ISBN, "EAN:", 13);
                }
            });
        }

        public List<ProductDetail> ParseProducts(string relativeUrl)
        {
            var resp = new List<ProductDetail>();
            List<Product> products = GetProducts(relativeUrl);

            if (products.Count > 0)
            {
                Parallel.ForEach(products, new ParallelOptions() { MaxDegreeOfParallelism = 100 }, product =>
                  {
                      if (GetCount(product.Name) < 1100)
                      {
                          resp.AddRange(GetItems(product.Url));
                      }
                      else
                      {
                          resp.AddRange(ParseProducts(product.Url));
                      }
                  });
            }
            else
            {
                resp.AddRange(GetItems(relativeUrl));
            }

            return resp;
        }

        private List<ProductDetail> GetItems(string relativeUrl)
        {
            var resp = new List<ProductDetail>();

            var bsasePage = helper.Get(Configuration.BaseUrl + relativeUrl + "?liststep=120");

            if (bsasePage != null)
            {
                var config = StructuredDataConfig.ParseJsonString(File.ReadAllText(@"Config\list.json"));

                var openScraping = new StructuredDataExtractor(config);
                var scrapingResults = openScraping.Extract(bsasePage);

                var json = JsonConvert.SerializeObject(scrapingResults, Formatting.Indented);

                var parsedJson = JsonConvert.DeserializeObject<List<string>>(json) ?? new List<string>();

                foreach (var item in parsedJson)
                {
                    resp.Add(new ProductDetail() { Url = item });
                }

                var pconfig = StructuredDataConfig.ParseJsonString(File.ReadAllText(@"Config\paging.json"));

                var popenScraping = new StructuredDataExtractor(pconfig);
                var pscrapingResults = popenScraping.Extract(bsasePage);

                var pjson = JsonConvert.SerializeObject(pscrapingResults, Formatting.Indented);

                var pages = JsonConvert.DeserializeObject<List<string>>(pjson);

                if (pages.Count > 0)
                {
                    foreach (var page in pages)
                    {
                        var htm = helper.Get(Configuration.BaseUrl + relativeUrl);

                        if (htm != null)
                        {
                            var htmConfig = StructuredDataConfig.ParseJsonString(File.ReadAllText(@"Config\list.json"));

                            var htmopenScraping = new StructuredDataExtractor(htmConfig);
                            var htmscrapingResults = htmopenScraping.Extract(htm);

                            var htmjson = JsonConvert.SerializeObject(htmscrapingResults, Formatting.Indented);

                            var htmlitm = JsonConvert.DeserializeObject<List<string>>(htmjson);

                            foreach (var item in htmlitm)
                            {
                                resp.Add(new ProductDetail() { Url = item });
                            }
                        }
                    }
                }
            }
            else
            {

            }
            return resp;
        }

        private List<Product> GetProducts(string relativeUrl)
        {
            var bsasePage = helper.Get(Configuration.BaseUrl + relativeUrl);

            if (bsasePage != null)
            {
                var config = StructuredDataConfig.ParseJsonString(File.ReadAllText(@"Config\categories.json"));

                var openScraping = new StructuredDataExtractor(config);
                var scrapingResults = openScraping.Extract(bsasePage);

                var json = JsonConvert.SerializeObject(scrapingResults, Formatting.Indented);

                var parsedJson = JsonConvert.DeserializeObject<List<Product>>(json);
                return parsedJson;
            }
            else
            {

            }
            return new List<Product>();
        }

        private int GetCount(string name)
        {
            var num = name.Substring(name.IndexOf('(') + 1);
            return int.Parse(num.Replace(")", string.Empty));
        }

        private string GetISBNrEAN(string[] input, string key, int length)
        {
            if (input == null)
            {
                return string.Empty;
            }

            foreach (var dt in input)
            {
                var str = dt.SafeTrim().Split(',').Where(s => s.ToUpper().Contains(key)).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(str))
                {
                    return str.Substring(str.IndexOf(key) + key.Length, length).SafeTrim();
                }
            }

            return string.Empty;
        }
    }
}
